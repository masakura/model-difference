﻿using System.Collections.Generic;
using System.Linq;
using DataModels.DataAccess.Infrastructure;
using DataModels.DataAccess.Models;
using DummyData;

namespace DataModels.DataAccess
{
    internal sealed class DataImporter : IDataImporter
    {
        private readonly ApplicationDbContext _db;

        public DataImporter(ApplicationDbContext db)
        {
            _db = db;
        }

        public void AddRange(IEnumerable<CustomerRow> rows)
        {
            var customers = rows.Select(row => new Customer {Name = row.Name, BirthDate = row.BirthDate});

            _db.Customers.AddRange(customers);
            _db.SaveChanges();
        }
    }
}