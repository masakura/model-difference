﻿using System;

namespace DataModels.DataAccess.Models
{
    public sealed class Customer
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public DateTime BirthDate { get; set; }
        public int Age { get; set; }
    }
}