﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataModels.DataAccess.Application;
using DataModels.DataAccess.Models;

namespace DataModels.DataAccess.Infrastructure
{
    internal sealed class CustomerDataSource : ICustomerRepository
    {
        private readonly ApplicationDbContext _db;

        public CustomerDataSource(ApplicationDbContext db)
        {
            _db = db;
        }

        public IEnumerable<Customer> All()
        {
            var customers = _db.Customers.ToArray();

            foreach (var customer in customers)
            {
                var today = DateTime.Today;
                var age = today.Year - customer.BirthDate.Year;

                if (today.Month < customer.BirthDate.Month ||
                    today.Month == customer.BirthDate.Month && today.Day < customer.BirthDate.Day)
                {
                    age -= 1;
                }

                customer.Age = age;
            }

            return customers;
        }
    }
}