﻿using DataModels.DataAccess.Models;
using Microsoft.EntityFrameworkCore;

namespace DataModels.DataAccess.Infrastructure
{
    public sealed class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Customer> Customers { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Customer>()
                .Ignore(e => e.Age);
        }
    }
}