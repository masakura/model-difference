﻿using DataModels.DataAccess.Application;
using Microsoft.AspNetCore.Mvc;

namespace DataModels.DataAccess.Controllers
{
    public sealed class CustomersController : Controller
    {
        private readonly ICustomerService _customerService;

        public CustomersController(ICustomerService customerService)
        {
            _customerService = customerService;
        }

        public IActionResult Index()
        {
            return View(_customerService.All());
        }
    }
}