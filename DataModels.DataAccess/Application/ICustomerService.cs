﻿using System.Collections.Generic;
using DataModels.DataAccess.Models;

namespace DataModels.DataAccess.Application
{
    public interface ICustomerService
    {
        IEnumerable<Customer> All();
    }
}