﻿using System.Collections.Generic;
using DataModels.DataAccess.Models;

namespace DataModels.DataAccess.Application
{
    internal sealed class CustomerService : ICustomerService
    {
        private readonly ICustomerRepository _customerRepository;

        public CustomerService(ICustomerRepository customerRepository)
        {
            _customerRepository = customerRepository;
        }

        public IEnumerable<Customer> All()
        {
            return _customerRepository.All();
        }
    }
}