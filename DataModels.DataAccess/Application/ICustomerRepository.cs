﻿using System.Collections.Generic;
using DataModels.DataAccess.Models;

namespace DataModels.DataAccess.Application
{
    public interface ICustomerRepository
    {
        IEnumerable<Customer> All();
    }
}