﻿using DummyData;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;

namespace DataModels.DataAccess
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = CreateWebHostBuilder(args).Build();

            host.ImportData();

            host.Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args)
        {
            return WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>();
        }
    }
}