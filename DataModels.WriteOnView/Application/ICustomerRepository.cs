﻿using System.Collections.Generic;
using DataModels.WriteOnView.Models;

namespace DataModels.WriteOnView.Application
{
    public interface ICustomerRepository
    {
        IEnumerable<Customer> All();
    }

    public interface ICustomerService
    {
        IEnumerable<Customer> All();
    }
}