﻿using System.Collections.Generic;
using System.Linq;
using DataModels.WriteOnView.Models;

namespace DataModels.WriteOnView.Application
{
    internal sealed class CustomerService : ICustomerService
    {
        private readonly ICustomerRepository _customerRepository;

        public CustomerService(ICustomerRepository customerRepository)
        {
            _customerRepository = customerRepository;
        }

        public IEnumerable<Customer> All()
        {
            return _customerRepository.All().ToArray();
        }
    }
}