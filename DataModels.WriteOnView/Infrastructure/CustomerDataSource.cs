﻿using System.Collections.Generic;
using System.Linq;
using DataModels.WriteOnView.Application;
using DataModels.WriteOnView.Models;

namespace DataModels.WriteOnView.Infrastructure
{
    internal sealed class CustomerDataSource : ICustomerRepository
    {
        private readonly ApplicationDbContext _db;

        public CustomerDataSource(ApplicationDbContext db)
        {
            _db = db;
        }

        public IEnumerable<Customer> All()
        {
            return _db.Customers.AsEnumerable();
        }
    }
}