﻿using DataModels.WriteOnView.Models;
using Microsoft.EntityFrameworkCore;

namespace DataModels.WriteOnView.Infrastructure
{
    public sealed class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Customer> Customers { get; set; }
    }
}