﻿using DataModels.WriteOnView.Application;
using Microsoft.AspNetCore.Mvc;

namespace DataModels.WriteOnView.Controllers
{
    public sealed class CustomersController : Controller
    {
        private readonly ICustomerService _customerService;

        public CustomersController(ICustomerService customerService)
        {
            _customerService = customerService;
        }

        public IActionResult Index()
        {
            return View(_customerService.All());
        }
    }
}