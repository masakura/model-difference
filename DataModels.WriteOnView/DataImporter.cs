﻿using System.Collections.Generic;
using System.Linq;
using DataModels.WriteOnView.Infrastructure;
using DataModels.WriteOnView.Models;
using DummyData;

namespace DataModels.WriteOnView
{
    internal sealed class DataImporter : IDataImporter
    {
        private readonly ApplicationDbContext _db;

        public DataImporter(ApplicationDbContext db)
        {
            _db = db;
        }

        public void AddRange(IEnumerable<CustomerRow> rows)
        {
            var customers = rows.Select(row => new Customer {Name = row.Name, BirthDate = row.BirthDate});

            _db.Customers.AddRange(customers);
            _db.SaveChanges();
        }
    }
}