﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using CsvHelper;

namespace DummyData
{
    internal sealed class CsvLoader : IDisposable
    {
        private readonly Stream _stream;

        public CsvLoader()
        {
            _stream = typeof(ImportDataExtensions).Assembly.GetManifestResourceStream(
                "DummyData.personal_infomation.csv");
        }

        public void Dispose()
        {
            _stream?.Dispose();
        }

        public IEnumerable<CustomerRow> Load()
        {
            using (var reader = new StreamReader(_stream))
            using (var csv = new CsvReader(reader))
            {
                csv.Configuration.RegisterClassMap<CustomerClassMap>();
                return csv.GetRecords<CustomerRow>().ToArray();
            }
        }
    }
}