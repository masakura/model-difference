﻿using CsvHelper.Configuration;

namespace DummyData
{
    internal sealed class CustomerClassMap : ClassMap<CustomerRow>
    {
        public CustomerClassMap()
        {
            Map(m => m.Name).Name("氏名");
            Map(m => m.BirthDate).Name("生年月日");
        }
    }
}