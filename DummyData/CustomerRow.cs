﻿using System;

namespace DummyData
{
    public sealed class CustomerRow
    {
        public string Name { get; set; }
        public DateTime BirthDate { get; set; }
    }
}