﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;

namespace DummyData
{
    public static class ImportDataExtensions
    {
        public static void ImportData(this IWebHost host)
        {
            using (var scope = host.Services.CreateScope())
            using (var loader = new CsvLoader())
            {
                var importer = scope.ServiceProvider.GetRequiredService<IDataImporter>();
                importer.AddRange(loader.Load());
            }
        }
    }
}