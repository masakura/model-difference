﻿using System.Collections.Generic;

namespace DummyData
{
    public interface IDataImporter
    {
        void AddRange(IEnumerable<CustomerRow> rows);
    }
}