﻿using System.Collections.Generic;
using ObjectModels.Models;

namespace ObjectModels.Application
{
    public interface ICustomerService
    {
        IEnumerable<Customer> All();
    }
}