﻿using System.Collections.Generic;
using ObjectModels.Models;

namespace ObjectModels.Application
{
    public interface ICustomerRepository
    {
        IEnumerable<Customer> All();
    }
}