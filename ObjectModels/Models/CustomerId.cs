﻿using System;

namespace ObjectModels.Models
{
    public sealed class CustomerId : IEquatable<CustomerId>
    {
        public CustomerId(Guid value)
        {
            if (value.Equals(Guid.Empty)) throw new ArgumentException();

            Value = value;
        }

        public Guid Value { get; }

        public bool Equals(CustomerId other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Value.Equals(other.Value);
        }

        public override bool Equals(object obj)
        {
            return ReferenceEquals(this, obj) || obj is CustomerId other && Equals(other);
        }

        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }

        public static CustomerId NewId()
        {
            return new CustomerId(Guid.NewGuid());
        }

        public override string ToString()
        {
            return $"{Value}";
        }
    }
}