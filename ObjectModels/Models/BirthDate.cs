﻿using System;

namespace ObjectModels.Models
{
    public sealed class BirthDate
    {
        public DateTime Value { get; }

        public BirthDate(DateTime value)
        {
            if (value.TimeOfDay != TimeSpan.Zero) throw new ArgumentException();

            Value = value;
        }

        public override string ToString()
        {
            return $"{Value:yyyy年MM月dd日}";
        }

        public Age Age()
        {
            return new Age(this);
        }

        internal int Year()
        {
            return Value.Year;
        }

        public bool IsBirthdayComing()
        {
            var today = DateTime.Today;

            if (today.Month < Value.Month) return false;
            if (today.Month > Value.Month) return true;

            return today.Day >= Value.Day;
        }
    }
}