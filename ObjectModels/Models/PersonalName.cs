﻿namespace ObjectModels.Models
{
    public sealed class PersonalName
    {
        private readonly string _value;

        public PersonalName(string value)
        {
            _value = value;
        }

        public override string ToString()
        {
            return $"{_value}";
        }
    }
}