﻿using System;

namespace ObjectModels.Models
{
    public sealed class Customer
    {
        public CustomerId Id { get; set; }
        public PersonalName Name { get; set; }
        public BirthDate BirthDate { get; set; }

        public Age Age()
        {
            return BirthDate.Age();
        }

        public static Customer New(string name, DateTime birthDate)
        {
            return new Customer
            {
                Id = CustomerId.NewId(),
                Name = new PersonalName(name),
                BirthDate = new BirthDate(birthDate)
            };
        }
    }
}