﻿using System;

namespace ObjectModels.Models
{
    public sealed class Age
    {
        private readonly BirthDate _birthDate;

        internal Age(BirthDate birthDate)
        {
            _birthDate = birthDate;
        }

        internal int AsInt32()
        {
            var today = DateTime.Today;
            var age = today.Year - _birthDate.Year();

            if (!_birthDate.IsBirthdayComing()) age -= 1;

            return age;
        }

        public override string ToString()
        {
            return $"{AsInt32()} 歳";
        }
    }
}