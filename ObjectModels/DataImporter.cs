﻿using System;
using System.Collections.Generic;
using System.Linq;
using DummyData;
using ObjectModels.Infrastructure;
using ObjectModels.Models;

namespace ObjectModels
{
    internal sealed class DataImporter : IDataImporter
    {
        private readonly ApplicationDbContext _db;

        public DataImporter(ApplicationDbContext db)
        {
            _db = db;
        }

        public void AddRange(IEnumerable<CustomerRow> rows)
        {
            var customers = rows.Select(row => Customer.New(row.Name, row.BirthDate));

            _db.AddRange(customers);
            _db.SaveChanges();
        }
    }
}