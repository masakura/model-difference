﻿using System;
using System.Collections.Generic;
using System.Linq;
using ObjectModels.Application;
using ObjectModels.Models;

namespace ObjectModels.Infrastructure
{
    internal sealed class CustomerDataSource : ICustomerRepository
    {
        private readonly ApplicationDbContext _db;

        public CustomerDataSource(ApplicationDbContext db)
        {
            _db = db;
        }

        public IEnumerable<Customer> All()
        {
            return _db.Customers.AsEnumerable();
        }
    }
}