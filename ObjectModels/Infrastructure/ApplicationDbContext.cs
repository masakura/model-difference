﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using ObjectModels.Models;

namespace ObjectModels.Infrastructure
{
    public sealed class ApplicationDbContext : DbContext
    {
        private static readonly ValueConverter<CustomerId, Guid> CustomerIdValueConverter =
            new ValueConverter<CustomerId, Guid>(v => v.Value, v => new CustomerId(v));

        private static readonly ValueConverter<BirthDate, DateTime> BirthDateValueConverter =
            new ValueConverter<BirthDate, DateTime>(v => v.Value, v => new BirthDate(v));

        private static readonly ValueConverter<PersonalName, string> PersonalNameValueConverter =
            new ValueConverter<PersonalName, string>(v => v.ToString(), v => new PersonalName(v));

        public ApplicationDbContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Customer> Customers { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            var customer = modelBuilder.Entity<Customer>();
            customer.Property(e => e.Id).HasConversion(CustomerIdValueConverter);
            customer.Property(e => e.Name).HasConversion(PersonalNameValueConverter);
            customer.Property(e => e.BirthDate).HasConversion(BirthDateValueConverter);
        }
    }
}