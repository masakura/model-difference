﻿using Microsoft.AspNetCore.Mvc;
using ObjectModels.Application;

namespace ObjectModels.Controllers
{
    public sealed class CustomersController : Controller
    {
        private readonly ICustomerService _customerService;

        public CustomersController(ICustomerService customerService)
        {
            _customerService = customerService;
        }

        public IActionResult Index()
        {
            return View(_customerService.All());
        }
    }
}